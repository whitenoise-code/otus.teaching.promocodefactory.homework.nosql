﻿using Humanizer;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Helpers;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer: IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IMongoDbOptions options)
        {
            var client = new MongoClient(options.ConnectionString);
            _database = client.GetDatabase(options.DatabaseName);
        }


        public void InitializeDb()
        {
            var preferenceCollectionName = nameof(Preference).Pluralize();
            _database.DropCollection(preferenceCollectionName);
            var preferences =
                _database.GetCollection<Preference>(preferenceCollectionName);
            preferences.InsertMany(FakeDataFactory.Preferences);

            var customersCollectionName = nameof(Customer).Pluralize();
            _database.DropCollection(customersCollectionName);
            var customers = _database.GetCollection<Customer>(customersCollectionName);
            customers.InsertMany(FakeDataFactory.Customers);


        }
    }
}