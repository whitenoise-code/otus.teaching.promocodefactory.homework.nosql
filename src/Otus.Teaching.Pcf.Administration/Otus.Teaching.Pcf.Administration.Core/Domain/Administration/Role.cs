﻿using Otus.Teaching.Pcf.Administration.Core.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    [BsonCollection("Roles")]
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}