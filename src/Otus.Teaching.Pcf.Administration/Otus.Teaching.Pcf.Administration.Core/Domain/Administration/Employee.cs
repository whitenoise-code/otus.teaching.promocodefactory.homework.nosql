﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Otus.Teaching.Pcf.Administration.Core.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    [BsonCollection("Employees")]
    public sealed class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public Guid RoleId { get; set; }

        public Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}