﻿using Otus.Teaching.Pcf.Administration.MongoDataAccess;

namespace Otus.Teaching.Pcf.Administration.WebHost.Options
{
    public class MongoDbOptions : IMongoDbOptions
    {
        public const string OptionsName = "MongoOptions";
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}